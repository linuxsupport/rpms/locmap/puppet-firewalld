Name:           puppet-firewalld
Version:        2.1
Release:        1%{?dist}
Summary:        Puppet module for firewalld

Group:          CERN/Utilities
License:        BSD
URL:            http://linux.cern.ch
Source0:        %{name}-%{version}.tgz

BuildArch:      noarch

Requires:       puppet-stdlib
Requires:       puppet-agent

%description
Puppet module for configuring firewalld in Centos 7.

%prep
%setup -q

%build
CFLAGS="%{optflags}"

%install
rm -rf %{buildroot}
install -d %{buildroot}/%{_datadir}/puppet/modules/firewalld/
cp -ra code/* %{buildroot}/%{_datadir}/puppet/modules/firewalld/
touch %{buildroot}/%{_datadir}/puppet/modules/firewalld/supporting_module

%files -n puppet-firewalld
%{_datadir}/puppet/modules/firewalld
%doc code/README.md

%changelog
* Tue Oct 15 2024 CERN Linux Droid <linux.ci@cern.ch> - 2.1-1
- Rebased to #7276f2c1 by locmap-updater

* Fri Dec 02 2022 Ben Morrice <ben.morrice@cern.ch> 2.0-4
- Bump release for disttag change

* Tue Feb 23 2021 Ben Morrice <ben.morrice@cern.ch> - 2.0-3
- fix requires for puppet-agent

* Thu Jan 09 2020 Ben Morrice <ben.morrice@cern.ch> - 2.0-2
- rebuild for el8

* Thu May 03 2018 Thomas Oulevey <thomas.oulevey@cern.ch> - 2.0-1
- rebuild for 7.5 release

* Tue Nov 29 2016 Thomas Oulevey <thomas.oulevey@cern.ch> - 0.2-1
- Rebuild for 7.3 release

* Fri Jun 17 2016 Aris Boutselis <aris.boutselis@cern.ch>
- Initial release
